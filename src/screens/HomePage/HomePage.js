import React from "react";
import styles from "./HomePage.styl";
import ToolTip from "../../components/ToolTip/ToolTip";

class HomePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentSelected: null,
      showBox: false,
      boxText: ""
    };

    this.clickAllowedClass = "click-allowed";
    this.buttonList = [
      {
        text: "Button A"
      },
      {
        text: "Button B"
      }
    ];
  }
  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside);
    document.addEventListener("keydown", this.escFunction.bind(this), false);
  }
  escFunction(event) {
    if (event.keyCode === 27) {
      this.setState({ showBox: false });
    }
  }
  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
    document.removeEventListener("keydown", this.escFunction, false);
  }
  handleClickOutside = elm => {
    console.log(elm.srcElement.classList);
    const classList = elm.srcElement.classList;
    const length = classList.length;
    let isKeepOpen = false;
    for (let i = 0; i < length; i++) {
      if (classList[i] == this.clickAllowedClass) {
        isKeepOpen = true;
        i = length;
      }
    }
    if (isKeepOpen == false) {
      this.setState({ showBox: false });
    }
  };
  buttonClick(item) {
    this.setState({ showBox: true, boxText: item.text, refToButton: item.refToButton });
  }
  render() {
    return (
      <React.Fragment>
        <div className={styles.container}>
          <ToolTip isShow={this.state.showBox} refToButton={this.state.refToButton} text={this.state.boxText} />
          <h1 className={styles.title}>Angular Test in React</h1>
          <section className={styles.buttonContainer}>
            {this.buttonList.map((item, index) => {
              console.log("re-rendering");
              return (
                <div key={index} style={{ position: "relative" }}>
                  <button
                    ref={ref => {
                      this.buttonList[index].refToButton = ref;
                    }}
                    onClick={this.buttonClick.bind(this, item)}
                    className="btn btn-primary click-allowed"
                  >
                    {item.text}
                  </button>
                </div>
              );
            })}
          </section>
        </div>
      </React.Fragment>
    );
  }
}

export default HomePage;
