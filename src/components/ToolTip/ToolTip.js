import React from "react";
// import ReactDOM from 'react-dom';
import styles from "./ToolTip.styl";

class ToolTip extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    console.log(this.props);
    this.attachToRef(this.props.refToButton);
  }
  componentDidUpdate(props) {
    this.attachToRef(this.props.refToButton);
  }
  attachToRef(buttonElm) {
    if (this.props.isShow && buttonElm) {
      // ReactDOM.render(this.messageBox, buttonElm.parentElement)
      buttonElm.parentElement.append(this.messageBox);
    }
  }
  render() {
    return (
      <React.Fragment>
        <div
          ref={ref => {
            this.messageBox = ref;
          }}
          // ref={this.messageBox}
          className={[
            styles.overheadTooltipAbove,
            styles.overheadTooltip,
            "click-allowed",
            this.props.isShow ? {} : styles.hideBox
          ].join(" ")}
        >
          {this.props.text}
        </div>
      </React.Fragment>
    );
  }
}

export default ToolTip;
